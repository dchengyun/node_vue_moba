const express = require('express');
const router = express.Router();
const Category = require('../../db/model/Category.js');
const Hero = require('../../db/model/Hero.js');

router.get('/heroes/list',async(req,res) =>{
	const parent = await Category.findOne({
		name:'英雄'
	});
	//聚合查询
	const cats = await Category.aggregate([
		{$match:{parent:parent._id}},
		{
			$lookup:{
			from:'heros',
			localField:'_id',
			foreignField:'categories',
			as:'herosList'
		}
	},
	// {
	// 	$addFields:{
	// 		newsList:{$slice:['$newsList',5]}
	// 	}
	// }
	]);
	const subCats = cats.map(v => v._id);
	cats.unshift({
		name:'热门',
		herosList:await Hero.find().where({
			categories:{$in:subCats}
		}).limit(5).lean()
	})
	// cats.map(cat =>{
	// 	cat.herosList.map(news =>{
	// 		news.CategoryName = (cat.name ==='热门') ? news.categories[0].name : cat.name;
	// 		return news;
	// 	})
	// 	return cat;
	// })
	res.send(cats);
});

module.exports = router;