module.exports = ()=>{
	const inflection = require('inflection'); //格式化变量名
	return async (req,res,next) =>{
	const modelName = inflection.classify(req.params.resource);
	// console.log(ModelName)
	req.Model = require(`../db/model/${modelName}`);
	await next();
 }
}