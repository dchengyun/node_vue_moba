const express = require('express');
const router = express.Router();
const JWT = require('jsonwebtoken');
const authmiddware = require('../../middle/authmiddware.js');
// const AdminUser = require('../../db/model/AdminUser.js')
// const assert = require('http-assert');
/**
 *通用接口
 *2021-05-19 
 */

//创建分类
router.post('/',async (req,res)=>{
	const model = await req.Model.create(req.body);
	res.send(model);
});
//保存修改的数据
router.put('/:id',async (req,res)=>{
	const model = await req.Model.findByIdAndUpdate(req.params.id,req.body);
	res.send(model);
});
//根据ID删除分类
router.delete('/:id',async (req,res)=>{
	 await req.Model.findByIdAndDelete(req.params.id);
	 res.send({
	 	message:'删除成功'
	 })
});
//获取列表
// const SECRET= 'SPINDGFsdfgwerIUSedfeiwj';
router.get('/',authmiddware(),async (req,res)=>{
	const queryOptions ={};
	
	if(req.Model.modelName === 'category') {
		queryOptions.populate = 'parent';
	//bug
	}else if(req.Model.modelName === 'hero') {
		queryOptions.populate = 'categories';
	}
	const lists = await req.Model.find().setOptions(queryOptions).limit(100);
	res.send(lists);
})
//根据ID获取列表
router.get('/:id',async (req,res)=>{
	const idEdit = await req.Model.findById(req.params.id);
	res.send(idEdit);
});


//删除所有，谨慎操作
// router.get('/del',async (req,res)=>{
// 	const lists = await req.Model.remove();
// 	res.send('del ok');
// })
module.exports = router