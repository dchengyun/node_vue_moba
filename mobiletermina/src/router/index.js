import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from 'views/Main'
import Home from 'views/Home'
import Article from 'views/Article'
import Heros from 'views/Heros'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Main',
    component: Main,
    children:[
    {
    path: '/',
    name: 'Home',
    component: Home
  },
   {
    path: '/article/:id',
    name: 'Article',
    component: Article,
    props:true,
   },
   
    ]
  },
  {
    path: '/heros/:id',
    name: 'Heros',
    component: Heros,
    props:true,
   },
]

const router = new VueRouter({
  routes,
   mode:'history'
})

export default router
