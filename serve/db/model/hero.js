const mongoose = require('mongoose');

//英雄模型
let HeroSchema = new mongoose.Schema({
	name:{type:String},
	avatar:{type:String},
	banner:{type:String},
	title:{type:String},//别名
	//英雄分类
	categories:[{
		type:mongoose.SchemaTypes.ObjectId,
		ref:'category'
	}],
	//分数
	scores:{
		difficult:{type:Number},
		skills:{type:Number},
		attack:{type:Number},
		survive:{type:Number}
	},
	//技能
	skills:[
		{
			icon:{type:String},
			name:{type:String},
			delay:{type:String},
			consume:{type:String},
			description:{type:String},
			skilltips:{type:String}
		}
	],
	//出装1
	items1:[
		{
			type:mongoose.SchemaTypes.ObjectId,
			ref:'item'
		}
	],
	//出装2
	items2:[
		{
			type:mongoose.SchemaTypes.ObjectId,
			ref:'item'
		}
	],
	usageTips:{type:String},
	battleTips:{type:String},
	teamTips:{type:String},
	//英雄关系
	partners:[{
		hero:{
			type:mongoose.SchemaTypes.ObjectId,
			ref:'hero'
		},
		description:{type:String}
	}],
});

let Hero = mongoose.model('hero',HeroSchema);

module.exports = Hero;
