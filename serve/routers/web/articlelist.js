const express = require('express');
const router = express.Router();
const Article = require('../../db/model/Article.js');
router.get('/article/:id',async (req,res)=>{
	const data = await Article.findById(req.params.id).lean();
	data.related = await Article.find().where({
		categories:{$in:data.categories}
	}).limit(2)
	res.send(data);
})


module.exports = router;