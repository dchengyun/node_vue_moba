const mongoose = require('mongoose');

//分类模型
let CategorySchema = new mongoose.Schema({
	name:{type:String},
	parent:{
		type:mongoose.SchemaTypes.ObjectId,
		ref:'category'
	}
});

let Category = mongoose.model('category',CategorySchema);

module.exports = Category;
