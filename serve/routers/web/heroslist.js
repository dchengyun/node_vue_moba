const express = require('express');
const router = express.Router();
const Hero = require('../../db/model/Hero.js');
const Category = require('../../db/model/Category.js');
const Item = require('../../db/model/Item.js');
router.get('/heros/:id',async (req,res)=>{
	const data = await Hero.findById(req.params.id)
	.populate('categories partners.hero items1 items2') //需要把model全部引入
	.lean();
	// data.related = await Hero.find().where({
	// 	categories:{$in:data.categories}
	// }).limit(2)
	res.send(data);
})


module.exports = router;