const express = require('express');
const router = express.Router();
const AdminUser = require('../../db/model/AdminUser.js')
const bcrypt = require('bcryptjs');
const JWT = require('jsonwebtoken');
const assert = require('http-assert');

router.post('/login',async (req,res)=>{
	const {username,password} = req.body;
	const user = await AdminUser.findOne({
		username:username
	}).select('+password');
	//用户名校验
	assert(user,401,'用户不存在');
	// if(!user){
	// 	return res.status(422).send({
	// 		message:'用户不存在'
	// 	});
	// }
	//密码校验
	const isValid = bcrypt.compareSync(password,user.password)
	assert(isValid,401,'密码错误');
	// if(!isValid){
	// 	return res.status(422).send({
	// 		message:'密码错误'
	// 	});
	// }
	const SECRET= 'SPINDGFsdfgwerIUSedfeiwj';
	const token = JWT.sign({id:user._id},SECRET);
	res.send({token});
})

module.exports = router