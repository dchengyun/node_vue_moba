const express = require('express');
const router = express.Router();
const Category = require('../../db/model/Category.js');
const Article = require('../../db/model/Article.js');

router.get('/news/list',async(req,res) =>{
	const parent = await Category.findOne({
		name:'新闻资讯'
	});
	//聚合查询
	const cats = await Category.aggregate([
		{$match:{parent:parent._id}},
		{
			$lookup:{
			from:'articles',
			localField:'_id',
			foreignField:'categories',
			as:'newsList'
		}
	},
	// {
	// 	$addFields:{
	// 		newsList:{$slice:['$newsList',5]}
	// 	}
	// }
	]);
	const subCats = cats.map(v => v._id);
	cats.unshift({
		name:'热门',
		newsList:await Article.find().where({
			categories:{$in:subCats}
		}).populate('categories').limit(5).lean()
	})
	cats.map(cat =>{
		cat.newsList.map(news =>{
			news.CategoryName = (cat.name ==='热门')? news.categories[0].name : cat.name;
			return news;
		})
		return cat;
	})
	res.send(cats);
});

module.exports = router;