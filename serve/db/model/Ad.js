const mongoose = require('mongoose');

//英雄模型
let AdSchema = new mongoose.Schema({
	name:{type:String},
	aditems:[{
		url:{type:String},
		image:{type:String},
	}],
});

let Ad = mongoose.model('ad',AdSchema);

module.exports = Ad;
