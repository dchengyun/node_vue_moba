const mongoose = require('mongoose');

//文章模型
let ArticleSchema = new mongoose.Schema({
	title:{type:String},
	//文章分类
	categories:[{
		type:mongoose.SchemaTypes.ObjectId,
		ref:'category'
	}],
	body:{type:String},
},{timestamps:true});

let Article = mongoose.model('article',ArticleSchema);

module.exports = Article;
