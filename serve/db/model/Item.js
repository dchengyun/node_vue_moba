const mongoose = require('mongoose');

//分类模型
let ItemSchema = new mongoose.Schema({
	name:{type:String},
	icon:{type:String}
});

let Item = mongoose.model('item',ItemSchema);

module.exports = Item;
