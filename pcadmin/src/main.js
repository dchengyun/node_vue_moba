import Vue from 'vue'
import App from './App.vue'
import './plugins/element.js'
import router from './router'
import http from 'network/http.js'
Vue.config.productionTip = false

Vue.prototype.$http = http

//混入
// Vue.mixin({
// 	methods:{
// 		getAuthorization(){
// 			return {
// 				Authorization:`Bearer ${localStorage.token || ''}`
// 			}
// 		}
// 	}
// })

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
