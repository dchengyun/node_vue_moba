const express = require('express');
const router = express.Router();
const multer = require('multer'); //文件处理中间件
// const authmiddware = require('../../middle/authmiddware.js')
/*
*图片上传接口
 */

//uploads,图片保存路劲
const uploadIcon = multer({
	dest:__dirname+'/../../uploads/icon_image'
}); 
const httpURL = 'http://localhost:3000/uploads/icon_image/';
router.post('/upload',uploadIcon.single('file'),async (req,res)=>{
	const iconFile = req.file;
	iconFile.url = httpURL+`${iconFile.filename}`;
	res.send(iconFile);
});



module.exports = router