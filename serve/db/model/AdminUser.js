const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
//英雄模型
let AdimnUserSchema = new mongoose.Schema({
	username:{type:String},
	password:{
		type:String,
		select:false,
		set(val){
			return bcrypt.hashSync(val,10);
		}
	},
});

let AdimnUser = mongoose.model('adimnuser',AdimnUserSchema);

module.exports = AdimnUser;
