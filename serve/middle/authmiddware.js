
module.exports = ()=>{
	const AdminUser = require('../db/model/AdminUser.js')
	const assert = require('http-assert');
	const JWT = require('jsonwebtoken');
	const SECRET= 'SPINDGFsdfgwerIUSedfeiwj';
	return async (req,res,next)=>{
	//获取资源作token验证
	let token = String(req.headers.authorization || '').split(' ').pop();
	assert(token,401,'请先登录')
	const { id } = JWT.verify(token,SECRET);
	assert(id,401,'请先登录')
	req.user = await AdminUser.findById(id);
	assert(req.user,401,'请先登录')
	await next();
 }
}

