import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from 'views/Main'

import Login from 'views/loginpage/Login'

import CategoryEdit from 'views/CategoryEdit'
import CategoryList from 'views/CategoryList'
import GoodsEdit from 'views/GoodsEdit'
import GoodsList from 'views/GoodsList'
import HerosEdit from 'views/HerosEdit'
import HerosList from 'views/HerosList'

import ArticleEdit from 'views/ArticleEdit'
import ArticleList from 'views/ArticleList'

import AdEdit from 'views/AdEdit'
import AdList from 'views/AdList'

import AdminUserEdit from 'views/AdminUserEdit'
import AdminUserList from 'views/AdminUserList'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta:{isPublic:true},
  },
  {
    path: '/',
    name: 'main',
    component: Main,
    children:[
      {
        path: '/categories/create',
        name: 'categoryEdit',
        component: CategoryEdit
      },
      {
        path: '/categories/list',
        name: 'categorylist',
        component: CategoryList
      },
      {
        path: '/categories/edit/:id',
        name: 'categoryEdit',
        component: CategoryEdit,
        props:true
      },
      {
        path: '/items/create',
        name: 'goodsEdit',
        component: GoodsEdit
      },
       {
        path: '/items/list',
        name: 'goodslist',
        component: GoodsList
      },
      {
        path: '/items/edit/:id',
        name: 'goodsEdit',
        component: GoodsEdit,
        props:true
      },
      {
        path: '/heros/create',
        name: 'herosEdit',
        component: HerosEdit
      },
       {
        path: '/heros/list',
        name: 'heroslist',
        component: HerosList
      },
      {
        path: '/heros/edit/:id',
        name: 'herosEdit',
        component: HerosEdit,
        props:true
      },
      {
        path: '/articles/create',
        name: 'articleEdit',
        component: ArticleEdit
      },
       {
        path: '/articles/list',
        name: 'articlelist',
        component: ArticleList
      },
      {
        path: '/articles/edit/:id',
        name: 'articleEdit',
        component: ArticleEdit,
        props:true
      },
      {
        path: '/ads/create',
        name: 'adEdit',
        component: AdEdit
      },
       {
        path: '/ads/list',
        name: 'adlist',
        component: AdList
      },
      {
        path: '/ads/edit/:id',
        name: 'articleEdit',
        component: AdEdit,
        props:true
      },
      {
        path: '/admin_users/create',
        name: 'adminusersEdit',
        component: AdminUserEdit
      },
       {
        path: '/admin_users/list',
        name: 'adminuserslist',
        component: AdminUserList
      },
      {
        path: '/admin_users/edit/:id',
        name: 'adminusersEdit',
        component: AdminUserEdit,
        props:true
      },
    ]
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to,from,next)=>{
  if(!to.meta.isPublic && !localStorage.token){
    return next('/login');
  }
  next();
})

export default router
