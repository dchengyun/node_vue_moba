const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path')
// const inflection = require('inflection'); //格式化变量名
const db = require('./db/connect');
const resmiddleware = require('./middle/resmiddleware.js');
const createCategoryRouter = require('./routers/pcadmin/createCategory.js');
const uploadRouter = require('./routers/pcadmin/upload.js');
const loginRouter = require('./routers/pcadmin/login.js');
const webapiRouter = require('./routers/web/webapinews.js');
const heroesRouter = require('./routers/web/heroes.js');
const articleRouter = require('./routers/web/articlelist.js');
const herosRouter = require('./routers/web/heroslist.js');
app.use(cors());
// 解析 application/json
app.use(bodyParser.json()); 
// 解析 application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

//通用接口
app.use('/admin/api/rest/:resource',resmiddleware(),createCategoryRouter)

//登录接口
app.use('/admin/api',loginRouter)

// 上传图片
app.use('/admin/api',uploadRouter);

//管理静态资源接口
app.use('/uploads/icon_image',express.static(path.join(__dirname+'/uploads/icon_image')));

//webapi
app.use('/web/api',webapiRouter);
app.use('/web/api',heroesRouter);
app.use('/web/api',articleRouter);
app.use('/web/api',herosRouter);
//统一处理错误
app.use(async(err,req,res,next)=>{
	res.status(err.statusCode).send({
		message:err.message
	})
}
)
app.listen(3000,(err)=>{
	if (err) {
		console.log(err);
	} else {
		console.log(`3000 is ok`);
	}
})