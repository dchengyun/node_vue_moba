# pcadmin

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


---
### 管理后台
1. 基于element-ui的后台管理界面搭建
```
vue add element
vue add router

```
2. 创建分类
- #服务端提交创建分类接口
```
http://localhost:3000/admin/api/rest/categories  (post)
```
3. 分类列表
```
http://localhost:3000/admin/api/rest/categories (get)
```
4. 修改分类
```
http://localhost:3000/admin/api/rest/categories/${this.id} (put)
```
5. 删除分类
```
http://localhost:3000/admin/api/rest/categories/${row._id}`(delete)
```
6. 通用接口
```
http://localhost:3000/admin/api/rest/:resource (categories/items)
```
---

7. 物品创建、查询、修改、删除
- 
8. 图片上传
```
http://localhost:3000/admin/api/upload (post)
```
9. 英雄管理
```
http://localhost:3000/admin/api/rest/heros (post)，
完成添加英雄功能
```
10. 技能管理
```
1.完成hero.js数据模型，
2.完成技能添加功能
```
11. 文章管理
```
1.页面搭建，
2.富文本编辑【图片上传】
<vue-editor useCustomImageHandler @image-added="handleImageAdded" v-model="form.body">
  --接口：http://localhost:3000/admin/api/upload (post)
```
12. 广告管理
```
1.页面搭建
```
13. 管理员账号管理
```
1. 管理员页面搭建
2. 登录页面搭建、接口
 -http://localhost:3000/admin/api/login (post)
3. 服务端登录校验
4. 客户端路由限制
 - befterEach()
```
